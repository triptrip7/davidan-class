import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { InvoicesService } from './invoices/invoices.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoiceComponent } from './invoices/invoice/invoice.component';

const appRoutes:Routes =[
  {path:'users' ,component:UsersComponent},
  {path:'invoices' ,component:InvoicesComponent},
  {path:'invoiceForm' ,component:InvoiceFormComponent},
  {path:'posts' ,component:PostsComponent},
  {path:'' ,component:InvoiceFormComponent},
  {path:'**' ,component:PageNotFoundComponent},
]

export const firebaseConfig = {
    apiKey: "AIzaSyDpRC33ObmKrk8LrVKjF1UQ-Hpz9jSrOs8",
    authDomain: "davidan-class.firebaseapp.com",
    databaseURL: "https://davidan-class.firebaseio.com",
    storageBucket: "davidan-class.appspot.com",
    messagingSenderId: "754677443588"
}

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    InvoicesComponent,
    InvoiceFormComponent,
    InvoiceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService, InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
