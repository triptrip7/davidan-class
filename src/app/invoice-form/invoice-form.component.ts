import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Invoice} from '../invoices/invoice/invoice';
import {NgForm} from '@angular/forms';
import {InvoicesService} from '../invoices/invoices.service';

@Component({
  selector: 'app-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {

  @Output() invoiceAddedEvent = new EventEmitter<Invoice>();

  invoice:Invoice = {name:'', amount:'1000'};

  addInvoice(invoice){
    this._invoicesService.addInvoice(invoice);
  }

  onSubmit(form:NgForm){
    this.addInvoice(this.invoice);
    console.log(this.invoice.name);
    console.log(this.invoice.amount);
    console.log(form);
    this.invoiceAddedEvent.emit(this.invoice);
    this.invoice = {name:'', amount:''}
  }

  constructor(private _invoicesService:InvoicesService) { }

  ngOnInit() {
  }

} 
